!(function($){
	$.fn.ccSlider = function(options) {
		return this.each(function(){
      var defaults = {
        time : 3500,		// 轮换秒数
        default : 1,    // 默认第几张
        navItemNumber : 4,// 导航有几张
        haveTitle : true // 导航是否有标题
      };
      
      var opt = $.extend({}, defaults, options);
      
      var slider = this,
          prefix = '.ccs-',
          picbox = $(prefix+'pic', slider),
          navbox = $(prefix+'nav', slider),
          navul = $('ul',navbox),
          prev = $(prefix+'prev', slider),
          next = $(prefix+'next', slider),
          len = picbox.find('li').length,
          boxwidth = picbox.width(),
          itemwidth = navbox.width() / opt.navItemNumber,
          picindex = --opt.default;
      
      function init() {
        createNavbox();
        picbox.css('width', boxwidth * len);
        picbox.find('li').css('width', boxwidth);
        bindEvent();
        hightlightPic(picindex);
      };
      
      // 创建导航使用data.smallpic和alt
      function createNavbox () {
        var htm = '';
        picbox.find('img').each(function(){
          htm += '<li><img src="' +this.dataset.smallpic+ '"/>';
          if (opt.haveTitle) {
            htm += '<br>' +this.alt;
          }
          htm += '</li>'
        });
        
        navul.append(htm)
            .css('width',itemwidth*len)
            .find('li').css('width',itemwidth);
      }
      
      // 大图高亮
      function hightlightPic (idx) {
        var w = boxwidth * idx;
        picbox.animate({
          'margin-left':'-' + w
        },opt.speed);
      }
      
      // 导航部分移动
      function moveNav (idx) {
        var w = itemwidth * idx;
        navul.animate({
          'margin-left':'-' + w
        },opt.speed);
      }
      
      // 绑定事件们
      function bindEvent () {
        // 导航向左翻
        prev.on('click', function() {
          if (picindex === 0) return;
          moveNav(--picindex);
        });
        // 导航向右翻
        next.on('click', function() {
          if ((picindex + opt.navItemNumber) === len) return;
          moveNav(++picindex);
        });
        // 点击导航图
        navbox.find('li')
          .on('mouseenter', function() {
          $(this).css('margin-top','-10px')
        }).on('mouseleave', function() {
          $(this).css('margin-top','0')
        }).on('click', function() {
          hightlightPic($(this).index());
        });
      }
      
      init();
      
    })
}})(jQuery);