var gulp = require('gulp');
var less = require('gulp-less');
var concat = require('gulp-concat');
var cssmin = require('gulp-cssmin');
var rename = require('gulp-rename');
//var browserSync = require('browser-sync');
//
//// start server
//gulp.task('browser-sync', function() {
//  browserSync({
//    server: {
//      baseDir: "./"
//    }
//  });
//});

gulp.task('less', function() {
  return gulp.src('./less/*.less')
    .pipe(less())
    .pipe(gulp.dest('./less/'))
});

gulp.task('cssmin', ['less'], function() {
  return gulp.src('./less/*.css')
    .pipe(concat('css.css'))
    .pipe(gulp.dest('./css/'))
    .pipe(cssmin())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest('./css/'))
});

gulp.task('watchless', function (){
  gulp.watch('less/*.less', ['cssmin']);
});

gulp.task('css', function (){
  return gulp.src('./less/*.less')
    .pipe(concat('css.less'))
    .pipe(less())
    .pipe(cssmin())
    .pipe(rename('css.min.css'))
    .pipe(gulp.dest('./css/'))
});
//
//gulp.task('default', ['browser-sync'], function () {
//  gulp.watch('less/*.less', ['css', browserSync.reload]);
//});